/**
 * File: src/SEA/FieldAdapter.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 01/10/15		hcai		created; for converting all field accesses to the setter/getter methods
 *							of the underlying field variables, for SEA implementation
 * 01/11/15		hcai		done coding; to be tested
 * 01/12/15		hcai		worked finally with Nanoxml after intensive debugging
*/
package SEA;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import profile.InstrumManager;

import dua.global.ProgramFlowGraph;
import dua.method.CFG;
import dua.method.CFGDefUses;
import dua.method.CFGDefUses.Def;
import dua.method.CFGDefUses.Use;
import dua.util.Util;

import soot.Body;
import soot.Local;
import soot.Modifier;
import soot.Scene;
import soot.SootClass;
import soot.SootField;
import soot.SootFieldRef;
import soot.SootMethod;
import soot.Type;
import soot.Value;
import soot.Unit;
import soot.VoidType;
import soot.jimple.AssignStmt;
import soot.jimple.FieldRef;
import soot.jimple.InstanceFieldRef;
import soot.jimple.InvokeExpr;
import soot.jimple.Jimple;
import soot.jimple.Stmt;
import soot.util.Chain;

import MciaUtil.utils;

import dua.Extension;
import dua.Forensics;

/**
 * A tool that converts all field accesses to corresponding setter/getter methods so that data flows are all realized through method calls,
 * thus enabling the SEA implementation
 * Step 1: identify all fields per class and create two methods one setter and the other getter for each field
 * Step 2: convert each field access to its corresponding setter/getter invocation
 * @author hcai
 */
public class FieldAdapter implements Extension {
	protected static SeaOptions opts = new SeaOptions();
	
	Map<SootClass, Set<SootField>> cls2fds = new HashMap<SootClass, Set<SootField>>(); // mapping from each class to its fields that need setters
	Map<SootClass, Set<SootField>> cls2fus = new HashMap<SootClass, Set<SootField>>(); // mapping from each class to its fields that need getters
	
	Map<SootField, Set<Def>> fld2defs = new HashMap<SootField, Set<Def>>(); // mapping from field to all its DEFs
	Map<SootField, Set<Use>> fld2uses = new HashMap<SootField, Set<Use>>(); // mapping from field to all its USEs
	
	Map<SootField, SootMethod> fld2setter = new HashMap<SootField, SootMethod>(); // mapping from field to its setter method
	Map<SootField, SootMethod> fld2getter = new HashMap<SootField, SootMethod>(); // mapping from field to its getter method
	
	public static void main(String args[]){
		args = preProcessArgs(opts, args);

		FieldAdapter fa = new FieldAdapter();
		// examine catch blocks
		dua.Options.ignoreCatchBlocks = false;
		// dua.Options.skipDUAAnalysis = true;
		
		Forensics.registerExtension(fa);
		Forensics.main(args);
	}
	
	protected static String[] preProcessArgs(SeaOptions _opts, String[] args) {
		opts = _opts;
		args = opts.process(args);
		
		String[] argsForDuaF;
		int offset = 0;

		argsForDuaF = new String[args.length + 2 - offset];
		System.arraycopy(args, offset, argsForDuaF, 0, args.length-offset);
		argsForDuaF[args.length+1 - offset] = "-paramdefuses";
		argsForDuaF[args.length+0 - offset] = "-keeprepbrs";
		
		return argsForDuaF;
	}
	
	public void run() {
		System.out.println("Running FieldAdapter extension of DUA-Forensics");
		//StmtMapper.getCreateInverseMap();
		
		collectFieldDefUses();
		creatSetters();
		creatGetters();
		
		convertFieldWrites();
		convertFieldReads();
		
		File fJimpleInsted = new File(Util.getCreateBaseOutPath() + "JimpleInstrumented.out");
		utils.writeJimple(fJimpleInsted);
	}
	
	/** phase 1: collect the field defs and uses of each class */
	private void collectFieldDefUses() {
		/* traverse all classes */
		Iterator<SootClass> clsIt = Scene.v().getClasses().iterator();
		while (clsIt.hasNext()) {
			SootClass sClass = (SootClass) clsIt.next();
			if ( sClass.isPhantom() ) {
				// skip phantom classes
				continue;
			}
			
			if ( !sClass.isApplicationClass() ) {
				// skip library classes
				continue;
			}
			
			/* traverse all methods of the class */
			Iterator<SootMethod> meIt = sClass.getMethods().iterator();
			while (meIt.hasNext()) {
				SootMethod sMethod = (SootMethod) meIt.next();
				if ( !sMethod.isConcrete() ) {
					// skip abstract methods and phantom methods, and native methods as well
					continue; 
				}
				if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
					// don't handle reflections now either
					continue;
				}
				
				// cannot instrument method event for a method without active body
				if ( !sMethod.hasActiveBody() ) {
					continue;
				}
				
				String meId = sMethod.getSignature();
				// -- DEBUG
				if (opts.debugOut()) {
					System.out.println("\n Field Write/Read analysis: now traversing method " + meId + "...");
				}
				
				// PatchingChain<Unit> pchn = sMethod.getActiveBody().getUnits();
				// Chain<Local> locals = body.getLocals();
				
				CFG cfg = ProgramFlowGraph.inst().getCFG(sMethod);
				CFGDefUses cfgDUs = (CFGDefUses)cfg;
				
				/*
				 * collect reader and writer methods of all static and instance variables
				 */
				List<Def> fldDefs = cfgDUs.getFieldDefs();
				List<Use> fldUses = cfgDUs.getFieldUses();
				for (Def d : fldDefs) {
					assert d.getValue() instanceof FieldRef; 
					SootFieldRef dfr = ((FieldRef)d.getValue()).getFieldRef();
					SootField df = ((FieldRef)d.getValue()).getField();
					assert df == dfr.resolve();
					if (/*df.isFinal() ||*/ df.isPhantom()) {
						continue;
					}
					if (df.isFinal() && !df.getDeclaringClass().isApplicationClass() && d.getN().getStmt()==null) {
						// library object fields (System.out, say) that do not have definition statement associated with 
						if (opts.debugOut()) {
							System.out.println("Skipped static final system object field def: " + d);
						}
						continue;
					}
					if (sMethod.getName().equals("<init>")) {
						if (dfr.name().equals("this$0")) {
							continue;
						}
						/*
						else {
							SootClass cls = df.getDeclaringClass(); 
							if (cls.getName().equalsIgnoreCase(sMethod.getDeclaringClass().getName()) && Utils.isAnonymousClass(cls)) {
								if (!df.isDeclared() || dfr.name().startsWith("val$")) {
									continue;
								}
							}
						}*/
					}
					else if (sMethod.getName().equals("<clinit>") && dfr.name().equals("class$0")) {
						continue;
					}
					
					SootClass cls = df.getDeclaringClass();
					if (cls.isPhantom() || cls.isLibraryClass() || cls.isPhantomClass()) {
						continue;
					}
					Set<SootField> fds = cls2fds.get(cls);
					if (null == fds) {
						fds = new HashSet<SootField>();
						cls2fds.put(cls, fds);
					}
					fds.add(df);
					
					Set<Def> ds = fld2defs.get(df);
					if (null == ds) {
						ds = new HashSet<Def>();
						fld2defs.put(df, ds);
					}
					ds.add(d);
				}
				
				for (Use u : fldUses) {
					assert u.getValue() instanceof FieldRef; 
					SootFieldRef ufr = ((FieldRef)u.getValue()).getFieldRef();
					SootField uf = ((FieldRef)u.getValue()).getField();
					assert uf == ufr.resolve();
					if (/*uf.isFinal() ||*/ uf.isPhantom()) {
						continue;
					}
					if (uf.isFinal() && !uf.getDeclaringClass().isApplicationClass()) {
						// library object fields (System.out, say) that do not have definition statement associated with 
						if (opts.debugOut()) {
							System.out.println("Skipped static final system object field use: " + u);
						}
						continue;
					}
					if (sMethod.getName().equals("<init>")) {
						if (ufr.name().equals("this$0")) {
							continue;
						}
						/*
						else {
							SootClass cls = df.getDeclaringClass(); 
							if (cls.getName().equalsIgnoreCase(sMethod.getDeclaringClass().getName()) && Utils.isAnonymousClass(cls)) {
								if (!df.isDeclared() || dfr.name().startsWith("val$")) {
									continue;
								}
							}
						}*/
					}
					else if (sMethod.getName().equals("<clinit>") && ufr.name().equals("class$0")) {
						continue;
					}
					
					SootClass cls = uf.getDeclaringClass();
					if (cls.isPhantom() || cls.isLibraryClass() || cls.isPhantomClass()) {
						continue;
					}
					Set<SootField> fus = cls2fus.get(cls);
					if (null == fus) {
						fus = new HashSet<SootField>();
						cls2fus.put(cls, fus);
					}
					fus.add(uf);
					
					Set<Use> us = fld2uses.get(uf);
					if (null == us) {
						us = new HashSet<Use>();
						fld2uses.put(uf, us);
					}
					us.add(u);
				} // for each field use
			} // for each method
		}
	} // - collectFieldDefUses
	
	/** create and add a setter method for each needy field to its declaring class */
	protected void creatSetters() {
		for (SootClass cls : cls2fds.keySet()) {
			/** traverse fields that need a setter method */
			for (SootField sf : cls2fds.get(cls)) {
				/** create a public setter member for each such field */
				String mname = "__hcai_set_" + sf.getName();
				List<Type> ptlist = new LinkedList<Type>();
				ptlist.add(sf.getType());
				int modifiers = Modifier.PUBLIC;
				if (sf.isStatic()) {
					modifiers |= Modifier.STATIC;
				}
				Type rettype = VoidType.v(); //new soot.VoidType(null); ---- the commented expression caused long nerve-wracking bugs
				SootMethod setter = new SootMethod(mname, ptlist, rettype, modifiers);
				Body by = Jimple.v().newBody();
				//setter.setDeclared(true); 
				setter.setDeclaringClass(cls);
	        	by.setMethod(setter);
	        	setter.setActiveBody(by);
	        	
	        	Chain<Local> locs = by.getLocals();
	        	Local othis = null;
	        	Stmt idstmtthis = null;
	        	if (!sf.isStatic()) {
	        		othis = Jimple.v().newLocal("r0", cls.getType());
	        		locs.add(othis);
	        		idstmtthis = Jimple.v().newIdentityStmt(othis, Jimple.v().newThisRef(cls.getType()));
	        	}
	        	Local opara = Jimple.v().newLocal("p0", sf.getType());
	        	locs.add(opara);
	        	
	        	Stmt idstmtpara = Jimple.v().newIdentityStmt(opara, Jimple.v().newParameterRef(sf.getType(), 0));
	        	
	        	FieldRef fr = null;
	        	if (sf.isStatic()) {
	        		fr = Jimple.v().newStaticFieldRef(sf.makeRef());
	        	}
	        	else {
	        		fr = Jimple.v().newInstanceFieldRef(othis, sf.makeRef());
	        	}
	        	
	        	Stmt astmt = Jimple.v().newAssignStmt(fr, opara);
	        	
	        	List<Unit> probes = new ArrayList<Unit>();
	        	if (!sf.isStatic()) {
	        		probes.add(idstmtthis);
	        	}
	        	probes.add(idstmtpara);
	        	probes.add(astmt);
	        	probes.add(Jimple.v().newReturnVoidStmt());
	        	
	        	//InstrumManager.v().insertProbeAtEntry(setter, probes);
	        	by.getUnits().addAll(probes);
	        	
	        	cls.addMethod(setter);
	        	
	        	fld2setter.put(sf, setter);
	        	
	        	if (opts.debugOut()) {
	        		System.out.println("a setter method " + setter + " added for field " + sf);
	        	}
			} // for each field
		} // for each class
	} // creatSetters
	
	/** create and add a getter method for each needy field to its declaring class */
	protected void creatGetters() {
		for (SootClass cls : cls2fus.keySet()) {
			/** traverse fields that need a getter method */
			for (SootField sf : cls2fus.get(cls)) {
				/** create a public setter member for each such field */
				String mname = "__hcai_get_" + sf.getName();
				List<Type> ptlist = new LinkedList<Type>();
				int modifiers = Modifier.PUBLIC;
				if (sf.isStatic()) {
					modifiers |= Modifier.STATIC;
				}
				Type rettype = sf.getType();
				SootMethod getter = new SootMethod(mname, ptlist, rettype, modifiers);
				Body by = Jimple.v().newBody();
				//getter.setDeclared(true); 
				getter.setDeclaringClass(cls);
	        	by.setMethod(getter);
	        	getter.setActiveBody(by);
	        	
	        	Chain<Local> locs = by.getLocals();
	        	Local othis = null;
	        	Stmt idstmtthis = null;
	        	if (!sf.isStatic()) {
	        		othis = Jimple.v().newLocal("r0", cls.getType());
	        		locs.add(othis);
	        		idstmtthis = Jimple.v().newIdentityStmt(othis, Jimple.v().newThisRef(cls.getType()));
	        	}
	        	Local otmp = Jimple.v().newLocal("t0", sf.getType());
	        	locs.add(otmp);
	        	
	        	FieldRef fr = null;
	        	if (sf.isStatic()) {
	        		fr = Jimple.v().newStaticFieldRef(sf.makeRef());
	        	}
	        	else {
	        		fr = Jimple.v().newInstanceFieldRef(othis, sf.makeRef());
	        	}
	        	
	        	Stmt astmt = Jimple.v().newAssignStmt(otmp, fr);
	        	Stmt retstmt = Jimple.v().newReturnStmt(otmp);
	        	
	        	List<Unit> probes = new ArrayList<Unit>();
	        	if (!sf.isStatic()) {
	        		probes.add(idstmtthis);
	        	}
	        	probes.add(astmt);
	        	probes.add(retstmt);
	        	
	        	//InstrumManager.v().insertProbeAtEntry(getter, probes);
	        	by.getUnits().addAll(probes);
	        	
	        	cls.addMethod(getter);
	        	
	        	fld2getter.put(sf, getter);
	        	
	        	if (opts.debugOut()) {
	        		System.out.println("a getter method " + getter + " added for field " + sf);
	        	}
			} // for each field
		} // for each class
	} // creatGetters
	
	protected void convertFieldWrites() {
		for (SootField sf : fld2defs.keySet()) {
			SootMethod setter = fld2setter.get(sf);
			assert setter != null;
			for (Def fd : fld2defs.get(sf)) {
				Stmt fdstmt = fd.getN().getStmt();
				if (!(fdstmt instanceof AssignStmt)) {
					System.err.println("non-assignment def for feild " + sf + " : cannot handle, skipped.");
					continue;
				}
				
				Value lv = ((AssignStmt)fdstmt).getLeftOp();
				Value rv = ((AssignStmt)fdstmt).getRightOp();
				assert lv == fd.getValue();
				
				SootMethod fdm = ProgramFlowGraph.inst().getContainingMethod(fd.getN());
				Body by = fdm.getActiveBody();
				
				List<Object> probes = new ArrayList<Object>();
				InvokeExpr invokeExpr = null;
				/*
				Local rvtmp = utils.createUniqueLocal(by, "_tmpW4_"+sf.getName(), rv.getType());
				Stmt astmt = Jimple.v().newAssignStmt(rvtmp, rv);
				probes.add(astmt);
				*/
				// Local rvtmp = (Local) utils.makeBoxedValue(fdm, rv, probes, rv.getType());
				
				if (sf.isStatic()) {
					invokeExpr = Jimple.v().newStaticInvokeExpr(setter.makeRef(), rv);
				}
				else {
					InstanceFieldRef insfr = (InstanceFieldRef)fd.getValue(); //sf.makeRef();
					Local lbase = (Local) insfr.getBase();
					// Local lbase = (Local) utils.makeBoxedValue(fdm, insfr.getBase(), probes, insfr.getBase().getType());
					// invokeExpr = Jimple.v().newSpecialInvokeExpr(lbase, setter.makeRef(), rv);
					//invokeExpr = Jimple.v().newVirtualInvokeExpr(lbase, setter.makeRef(), rvtmp);
					if (insfr.getField().getDeclaringClass().isInterface()) {
						invokeExpr = Jimple.v().newInterfaceInvokeExpr(lbase, setter.makeRef(), rv);
					}
					else {
						invokeExpr = Jimple.v().newVirtualInvokeExpr(lbase, setter.makeRef(), rv);
					}
				}
				Stmt invMofStmt = Jimple.v().newInvokeStmt(invokeExpr);
				probes.add(invMofStmt);
				
				InstrumManager.v().insertBeforeRedirect(by.getUnits(), probes, fdstmt);
				by.getUnits().remove(fdstmt);
				
				/*
				if (!fd.getN().getPreds().isEmpty()) {
					InstrumManager.v().insertAfter(by.getUnits(), probes, fd.getN().getPreds().get(0).getStmt());
				}
				else if (!fd.getN().getSuccs().isEmpty()) {
					InstrumManager.v().insertBeforeNoRedirect(by.getUnits(), probes, fd.getN().getSuccs().get(0).getStmt());
				}
				else {
					// rarely happens when there is no other statements in this method except for the field access itself
					for (Object o:probes) {
						by.getUnits().add((Unit)o);
					}
				}
				*/
			} // for each field writes
		} // for each field
	} // convertFieldWrites
	
	protected void convertFieldReads() {
		for (SootField sf : fld2uses.keySet()) {
			SootMethod getter = fld2getter.get(sf);
			assert getter != null;
			for (Use fu : fld2uses.get(sf)) {
				Stmt fustmt = fu.getN().getStmt();
				if (!(fustmt instanceof AssignStmt)) {
					System.err.println("non-assignment use for feild " + sf + " : cannot handle, skipped.");
					continue;
				}
				
				Value lv = ((AssignStmt)fustmt).getLeftOp();
				Value rv = ((AssignStmt)fustmt).getRightOp();
				assert rv == fu.getValue();
				
				SootMethod fum = ProgramFlowGraph.inst().getContainingMethod(fu.getN());
				Body by = fum.getActiveBody();
				Local ltmp = utils.createUniqueLocal(by, "_tmpR_"+sf.getName(), fu.getValue().getType()); //sf.getType());
				
				List<Object> probes = new ArrayList<Object>();
				InvokeExpr invokeExpr = null;
				if (sf.isStatic()) {
					invokeExpr = Jimple.v().newStaticInvokeExpr(getter.makeRef());
				}
				else {
					InstanceFieldRef insfr = (InstanceFieldRef)fu.getValue(); //sf.makeRef();
					Local lbase = (Local) insfr.getBase();
					//Local lbase = (Local)utils.makeBoxedValue(fum, insfr.getBase(), probes, insfr.getBase().getType());
					//invokeExpr = Jimple.v().newSpecialInvokeExpr(lbase, getter.makeRef());
					if (insfr.getField().getDeclaringClass().isInterface()) {
						invokeExpr = Jimple.v().newInterfaceInvokeExpr(lbase, getter.makeRef());
					} 
					else {  
						invokeExpr = Jimple.v().newVirtualInvokeExpr(lbase, getter.makeRef());
					}
				}
				Stmt astmt1 = Jimple.v().newAssignStmt(ltmp, invokeExpr);
				Stmt astmt2 = Jimple.v().newAssignStmt(lv, ltmp);
				probes.add(astmt1);
				probes.add(astmt2);
				
				InstrumManager.v().insertBeforeRedirect(by.getUnits(), probes, fustmt);
				by.getUnits().remove(fustmt);
				
			} // for each field writes
		} // for each field
	} // convertFieldWrites
}

/* vim :set ts=4 tw=4 tws=4 */
