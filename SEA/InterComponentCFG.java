/**
 * File: src/SEA/InterComponentCFG.java
 * -------------------------------------------------------------------------------------------
 * Date			Author      Changes
 * -------------------------------------------------------------------------------------------
 * 12/17/14		hcai		created; for constructing the interprocedural component CFG from ICFG
 *							which directly suppots SEA-based impact analysis
 * 12/20/14		hcai		the first (syntactically) working version 
 * 01/09/15		hcai		reached the working version (worked for all the four legacy subjects)
 * 01/14/15		hcai		field adaptation does not make difference: SEA still misses a lot of methods in the 
 * 							forward dependence sets compared to MDG and also to the static-forward-slice based ground truth
 * 							still struggling with SEA implementation: here added the modelLibFuncs option to choose if 
 * 							modeling all library methods (using phantom method and correspondig CFGs that contains only an ENTRY node) 
 * 01/16/15		hcai		finalized: the setters/getters are only needed for finding inter-class (class-level) dependencies and, for
 * 							that matter, only the setters and getters for global variables (class fields, i.e. static member variables)
 * 							and they have to be placed in a separate class; then such added class will bridge two classes in which one 
 * 							defines a gloabl variable and the other uses it without calling any method of each other.   
*/
package SEA;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import dua.global.ProgramFlowGraph;
import dua.method.CFG;
import dua.method.CallSite;

import soot.Body;
import soot.Modifier;
//import soot.PointsToAnalysis;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Type;
import soot.Unit;
import soot.VoidType;
import soot.jimple.Jimple;
import soot.jimple.Stmt;
//import soot.jimple.toolkits.callgraph.CallGraph;
//import soot.jimple.toolkits.callgraph.Edge;
//import soot.jimple.toolkits.callgraph.CallGraphBuilder;
import soot.toolkits.graph.DirectedGraph;
//import soot.toolkits.graph.StronglyConnectedComponentsFast;
//import soot.toolkits.graph.StronglyConnectedComponents;

import MciaUtil.CompleteUnitGraphEx;
import MciaUtil.InterCFGraphEx;
//import MciaUtil.utils;

/** copied from Soot-2.5.0 */
class StronglyConnectedComponentsFast<N>
{
	  protected final List<List<N>> componentList = new ArrayList<List<N>>();
	  protected final List<List<N>> trueComponentList = new ArrayList<List<N>>();
	
	  protected int index = 0;
	
	  protected Map<N,Integer> indexForNode, lowlinkForNode;
	
	  protected Stack<N> s;
	
	  protected DirectedGraph<N> g;
	    
	  /**
	   *  @param g a graph for which we want to compute the strongly
	   *           connected components. 
	   *  @see DirectedGraph
	   */
	  public StronglyConnectedComponentsFast(DirectedGraph<N> g)
	  {
	    this.g = g;
	    s = new Stack<N>();
	    List<N> heads = g.getHeads();
	
	    indexForNode = new HashMap<N, Integer>();
	    lowlinkForNode = new HashMap<N, Integer>();
	
	    for(Iterator<N> headsIt = heads.iterator(); headsIt.hasNext(); ) {
	      N head = headsIt.next();
	      if(!indexForNode.containsKey(head)) {
	        recurse(head);
	      }
	    }
	
	    //free memory
	    indexForNode = null;
	    lowlinkForNode = null;
	    s = null;
	    g = null;
	  }
	
	  protected void recurse(N v) {
	    indexForNode.put(v, index);
	    lowlinkForNode.put(v, index);
	    index++;
	    s.push(v);
	    for(N succ: g.getSuccsOf(v)) {
	      if(!indexForNode.containsKey(succ)) {
	        recurse(succ);
	        lowlinkForNode.put(v, Math.min(lowlinkForNode.get(v), lowlinkForNode.get(succ)));
	      } else if(s.contains(succ)) {
	        lowlinkForNode.put(v, Math.min(lowlinkForNode.get(v), indexForNode.get(succ)));
	      }			
	    }
	    if(lowlinkForNode.get(v).intValue() == indexForNode.get(v).intValue()) {
	      List<N> scc = new ArrayList<N>();
	      N v2;
	      do {
	        v2 = s.pop();
	        scc.add(v2);
	      }while(v!=v2);			
	      componentList.add(scc);
	      if(scc.size()>1) {
	        trueComponentList.add(scc);
	      } else {
	        N n = scc.get(0);
	        if(g.getSuccsOf(n).contains(n))
	          trueComponentList.add(scc);
	      }
	    }
	  }
	
	  /**
	   *   @return the list of the strongly-connected components
	   */
	  public List<List<N>> getComponents()
	  {
	    return componentList;
	  }
	
	  /**
	   *   @return the list of the strongly-connected components, but only those
	   *   that are true components, i.e. components which have more than one element
	   *   or consists of one node that has itself as a successor 
	   */
	  public List<List<N>> getTrueComponents()
	  {
	    return trueComponentList;
	  }
}
/**
 * An ICCFG builder that computes interprocedural component control flow graph from the given ICFG
 * Step 1: remove nodes other than call sites and entry/exit
 * Step 2: shrink/collapse strongly connected subgraphs into single nodes
 * @author hcai
 */
public class InterComponentCFG extends InterCFGraphEx {
	// provide facilities for SEA based impact-set querying
	protected Map<Unit,SootMethod> entryToMethod = new HashMap<Unit,SootMethod>();
	
	//private static CallGraph cg = null; 
	
	public InterComponentCFG(boolean _debug) {
		super(_debug);
	}
	public InterComponentCFG() {
		this(true);
	}

	@Override public String toString() {
		return "Exceptional Interprocedural Component CFG [Static] " + 
			getNumberOfNodes() + " nodes, " + getNumberOfEdges() + " edges ";
	}
	

	// retrieve the method of the given ENTRY node
	public SootMethod getMethodFromEntry(Unit u) {
		if (!entryToMethod.containsKey(u)) return null;
		return entryToMethod.get(u);
	}
	
	// get all nodes in the CCFG of the given method whose signature equals to the input method f
	public List<Unit> getComponents(SootMethod f) {
		Unit entry = getStart(f);
		List<Unit> ns = new ArrayList<Unit>();
		if (null == entry) {
			return ns;
		}
		CompleteUnitGraphEx cfg = this.getCFG(f);
		if (cfg == null) {
			return ns;
		}
		
		Iterator<Unit> it = cfg.iterator();
		//assert entry == cfg.ENTRY;
		if (entry != cfg.ENTRY){
			System.err.println(entry + " != " + cfg.ENTRY);
			assert false;
		}
		while (it.hasNext()) {
			Unit c = it.next();
			if (CompleteUnitGraphEx.isEntry((Stmt)c)) {
				// collect components only
				continue;
			}
			if (!ns.contains(c)) {
				ns.add(c);
			}
		}
		
		return ns;
	}
	
	Map<SootMethod, SootMethod> _cacheMethods = new HashMap<SootMethod, SootMethod>();
	private SootMethod getCreateVirtualmethod(SootMethod m, SootClass sClass) {
		if (_cacheMethods.containsKey(m)) {
			return _cacheMethods.get(m);
		}
		Body by = Jimple.v().newBody();
		List<Type> ptlist = new LinkedList<Type>();
		SootMethod sMethod = new SootMethod(m.getName(), ptlist, VoidType.v(), Modifier.PUBLIC | Modifier.STATIC);
    	SootClass cls = new SootClass(sClass.getName());
    	sMethod.setDeclared(true); 
    	sMethod.setDeclaringClass(cls);
    	by.setMethod(sMethod);
    	by.getUnits().add(Jimple.v().newReturnVoidStmt());
    	sMethod.setActiveBody(by);
    	_cacheMethods.put(m, sMethod);
    	
    	return sMethod;
	}
	private void addCfg(SootMethod sMethod) {
		if (mToCfg.containsKey(sMethod)) {
			return;
		}
		CompleteUnitGraphEx cfg = new CompleteUnitGraphEx(sMethod.getActiveBody());
		
		mToCfg.put(sMethod, cfg);
		entryToMethod.put(cfg.ENTRY, sMethod);
	}
	
	/** phase 1: collect the CFG of each method */
	Map<CallSite, Set<SootMethod>> csToCallees = new HashMap<CallSite, Set<SootMethod>>();
	private void collectCFGs() {
		/* traverse all classes */
		Iterator<SootClass> clsIt = Scene.v().getClasses().iterator();
		while (clsIt.hasNext()) {
			SootClass sClass = (SootClass) clsIt.next();
			if ( sClass.isPhantom() ) {
				// skip phantom classes
				continue;
			}
			
			if (modelLibfuncs) {
				if ( sClass.isLibraryClass() ) {
					// skip library classes
					continue;
				}
			}
			else {
				if ( !sClass.isApplicationClass() ) {
					// skip library classes
					continue;
				}
			}
			
			/* traverse all methods of the class */
			Iterator<SootMethod> meIt = sClass.getMethods().iterator();
			while (meIt.hasNext()) {
				SootMethod sMethod = (SootMethod) meIt.next();
				if ( !sMethod.isConcrete() ) {
					// skip abstract methods and phantom methods, and native methods as well
					if (!modelLibfuncs)
						continue; 
				}
				if ( sMethod.toString().indexOf(": java.lang.Class class$") != -1 ) {
					// don't handle reflections now either
					if (!modelLibfuncs)
						continue;
				}
				
				// cannot instrument method event for a method without active body
				if ( !sMethod.hasActiveBody() ) {
					if (!modelLibfuncs)
						continue;
				}
				if (!ProgramFlowGraph.inst().getReachableAppMethods().contains(sMethod)) {
					continue;
				}
				
				//Body by = null;
				if (sMethod.hasActiveBody()) {
					//by = sMethod.getActiveBody();
				}
				else {
					if (!modelLibfuncs)
						continue;
					sMethod = getCreateVirtualmethod(sMethod, sClass);
					//by = sMethod.getActiveBody();
				}
				
				addCfg(sMethod);
			}
		}
		
		// add library callees
		Set<SootMethod> toadd = new LinkedHashSet<SootMethod>();
		for (SootMethod m : mToCfg.keySet()) {
			// call edge: connecting call site to entry of each possible callee
			CFG cfg = ProgramFlowGraph.inst().getCFG(m);
			if (cfg == null) continue;
			List<CallSite> cses = cfg.getCallSites();
			for (CallSite cs : cses) {
				List<SootMethod> callees = cs.getAllCallees(); // cs.getAppCallees();
				/*
				List<SootMethod> cgcallees = new ArrayList<SootMethod>();
				Iterator<Edge> eit = cg.edgesOutOf(cs.getLoc().getStmt());
				while (eit.hasNext()) {
					Edge e = eit.next();
					cgcallees.add(e.getTgt().method());
				}
				callees.addAll(cgcallees);
				*/
				
				Set<SootMethod> ces = csToCallees.get(cs);
				if (ces == null) {
					ces = new LinkedHashSet<SootMethod>();
					csToCallees.put(cs, ces);
				}
				
				for (SootMethod ce : callees) {
					//ReachableUsesDefs rudce = (ReachableUsesDefs) ProgramFlowGraph.inst().getCFG(ce);
					if (!mToCfg.containsKey(ce)) {
						if (!modelLibfuncs) continue;
						
						if (!ce.hasActiveBody()) 
						{
							ce = getCreateVirtualmethod(ce, ce.getDeclaringClass());
						}
						toadd.add(ce);
					}
					
					ces.add(ce);
				}
			}
		}
		for (SootMethod mm : toadd) {
			addCfg(mm);
		}
		
		if (debugOut) 
		{
			for (SootMethod m : mToCfg.keySet()) {
				System.err.println(m);
			}
			System.err.println("size=" + mToCfg.size());
		}
	} // - collectCFGs
	
	/** phase 2: copy all CFGs to the ICCFG (this object) before adding call edges amongst them */
	private void copyCFGs() {
		unitToSuccs.clear();
		unitToPreds.clear();
		for (SootMethod m : mToCfg.keySet()) {
			CompleteUnitGraphEx cfg = mToCfg.get(m);
			Iterator<Unit> iter = cfg.iterator();
			while (iter.hasNext()) {
				Unit curu = iter.next();
				//System.out.println("\t" + curu + " has " + cfg.getSuccsOf(curu).size()+" descendants:");
				List<Unit> succs = unitToSuccs.get(curu);
				if (succs == null) {
					succs = new ArrayList<Unit>();
					unitToSuccs.put(curu, succs);
				}
				for (Unit u : cfg.getSuccsOf(curu)) {
					//System.out.println("\t\t"+ u + "");
					if (!succs.contains(u)) succs.add(u);
				}
				
				List<Unit> preds = unitToPreds.get(curu);
				if (preds == null) {
					preds = new ArrayList<Unit>();
					unitToPreds.put(curu, preds);
				}
				for (Unit u : cfg.getPredsOf(curu)) {
					//System.out.println("\t\t"+ u + "");
					if (!preds.contains(u)) preds.add(u);
				}
			}
		}
	} // - copyCCFGs
	
	/** phase 3: connect the CFGs with call edges (but not return edges, so the outcome is NOT a complete ICFG */
	private void addCallEdges() {
		/*
		for (SootMethod m : mToCfg.keySet()) {
			// call edge: connecting call site to entry of each possible callee
			CFG cfg = ProgramFlowGraph.inst().getCFG(m);
			List<CallSite> cses = cfg.getCallerSites();
			for (CallSite cs : cses) {
				List<SootMethod> appCallees = cs.getAllCallees(); // cs.getAppCallees();
				
				//if (!unitToSuccs.containsKey(cs.getLoc().getStmt())) {
				//	System.err.println("not found in " + m + " --- " + cs.getLoc().getStmt());
				//}
				assert unitToSuccs.containsKey(cs.getLoc().getStmt()); // the call site must be in the caller's CFG
				List<Unit> succs = unitToSuccs.get(cs.getLoc().getStmt());
				for (SootMethod ce : appCallees) {
					if (!ce.equals(cfg.getMethod())) {
						// connect the caller to the right callee
						continue;
					}
					//ReachableUsesDefs rudce = (ReachableUsesDefs) ProgramFlowGraph.inst().getCFG(ce);
					assert mToCfg.containsKey(ce);
					CompleteUnitGraphEx cecfg = mToCfg.get(ce);
					assert cecfg != null; // CFG for each method should have been created and cached
					
					if (!succs.contains(cecfg.ENTRY)) succs.add(cecfg.ENTRY);
					
					assert unitToPreds.containsKey(cecfg.ENTRY);
					List<Unit> preds = unitToPreds.get(cecfg.ENTRY);
					if (!preds.contains(cs.getLoc().getStmt())) preds.add(cs.getLoc().getStmt());
				}
			}
		}*/
		for (SootMethod m : mToCfg.keySet()) {
			// call edge: connecting call site to entry of each possible callee
			CFG cfg = ProgramFlowGraph.inst().getCFG(m);
			if (cfg == null) continue;
			List<CallSite> cses = cfg.getCallSites();
			for (CallSite cs : cses) {
				Set<SootMethod> callees = csToCallees.get(cs); // cs.getAllCallees(); // cs.getAppCallees();
				/*
				if (!unitToSuccs.containsKey(cs.getLoc().getStmt())) {
					System.err.println("not found in " + m + " --- " + cs.getLoc().getStmt());
				}*/
				assert unitToSuccs.containsKey(cs.getLoc().getStmt()); // the call site must be in the caller's CFG
				List<Unit> succs = unitToSuccs.get(cs.getLoc().getStmt());
				for (SootMethod ce : callees) {
					//ReachableUsesDefs rudce = (ReachableUsesDefs) ProgramFlowGraph.inst().getCFG(ce);
					assert (mToCfg.containsKey(ce));
					
					CompleteUnitGraphEx cecfg = mToCfg.get(ce);
					assert cecfg != null; // CFG for each method should have been created and cached
					
					// add the call edge
					if (!succs.contains(cecfg.ENTRY)) succs.add(cecfg.ENTRY);
					
					assert unitToPreds.containsKey(cecfg.ENTRY);
					List<Unit> preds = unitToPreds.get(cecfg.ENTRY);
					if (!preds.contains(cs.getLoc().getStmt())) preds.add(cs.getLoc().getStmt());
				}
			}
		}
	} // - addCallEdges
	
	protected void connectSetterToGetters() {
		Map<SootMethod, SootMethod> setter2getters = new HashMap<SootMethod, SootMethod>();
		String stoken = "__hcai_set_";
		String gtoken = "__hcai_get_";
		for (SootMethod ms : mToCfg.keySet()) {
			if (!ms.getName().startsWith(stoken)) continue;
			String sfld = ms.getName().substring(ms.getName().indexOf(stoken)+stoken.length());//, ms.getName().indexOf("("));
			for (SootMethod mg : mToCfg.keySet()) {
				if (!mg.getName().startsWith(gtoken)) continue;
				String gfld = mg.getName().substring(mg.getName().indexOf(gtoken)+gtoken.length()); //, mg.getName().indexOf("("));
				if (sfld.equals(gfld) && ms.getDeclaringClass()==mg.getDeclaringClass()) {
					setter2getters.put(ms, mg);
					break;
				}
			}
		}
		
		int icnt = 0;
		for (SootMethod ms : setter2getters.keySet()) {
			CompleteUnitGraphEx scfg = mToCfg.get(ms);
			SootMethod mg = setter2getters.get(ms);
			CompleteUnitGraphEx gcfg = mToCfg.get(mg);
			
			//System.out.println(ms + " ------------> " + mg);
			/*
			List<Unit> ssuccs = unitToSuccs.get(scfg.ENTRY);
			if (ssuccs == null) {
				ssuccs = new ArrayList<Unit>();
				unitToSuccs.put(scfg.ENTRY, ssuccs);
			}
			ssuccs.add(gcfg.ENTRY);
			
			List<Unit> gpreds = unitToPreds.get(gcfg.ENTRY);
			if (gpreds == null) {
				gpreds = new ArrayList<Unit>();
				unitToPreds.put(gcfg.ENTRY, gpreds);
			}
			gpreds.add(scfg.ENTRY);
			
			
			for (Unit a : unitToPreds.get(scfg.ENTRY)) {
				List<Unit> ssuccs = unitToSuccs.get(a);
				for (Unit b : unitToPreds.get(gcfg.ENTRY)) {
					if (!ssuccs.contains(b)) {
						ssuccs.add(b);
						icnt ++;
					}
				}
			}*/
			for (Unit a : unitToPreds.get(gcfg.ENTRY)) {
				List<Unit> gsuccs = unitToSuccs.get(a);
				for (Unit b : unitToPreds.get(scfg.ENTRY)) {
					if (!gsuccs.contains(b)) {
						gsuccs.add(b);
						icnt ++;
					}
				}
			}
		}
		System.out.println(icnt + " eges added");
	}
	
	/** phase 4: remove irrelevant nodes and shrink SCCs to single nodes in each CFG to finalize the ICCFG */
	private void createICCFG() {
		for (SootMethod m : mToCfg.keySet()) {
			CompleteUnitGraphEx cfg = mToCfg.get(m);
			
			// remove all nodes but entry and call sites from the cfg
			Iterator<Unit> iter = cfg.iterator();
			List<Unit> toRemove = new ArrayList<Unit>();
			while (iter.hasNext()) {
				Unit curu = iter.next();
				Stmt s = (Stmt)curu;
				if (! (s.containsInvokeExpr() || CompleteUnitGraphEx.isEntry(s)) ) {
					toRemove.add(curu);
				}
			}
			for (Unit u : toRemove) {
				cfg.removeGraphNode(u);
				assert this.hasNode(u);
				this.removeNode(u);
			}
			
			// shrink strongly connected components into single nodes (choose the first node of each component to shrink to)
			
			StronglyConnectedComponentsFast<Unit> sccs = new StronglyConnectedComponentsFast<Unit>(cfg);
			
			if (sccs.getTrueComponents().size()<=1) { continue; }
			for (List<Unit> scc : sccs.getTrueComponents()) {
			/*
			StronglyConnectedComponents sccs = new StronglyConnectedComponents(cfg);
			if (sccs.getComponents().size()<=1) { continue; }
			for (List<Unit> scc : sccs.getComponents()) {
			*/	
				// shrink the scc to its first node
				if (scc.size()<1) continue;
				
				List<Unit> suc2add = new ArrayList<Unit>();
				List<Unit> pred2add = new ArrayList<Unit>();
				for (int i = 1; i < scc.size(); i++) {
					Unit u = scc.get(i);
					
					suc2add.addAll(cfg.getSuccsOf(u));
					pred2add.addAll(cfg.getPredsOf(u));
					
					cfg.removeGraphNode(u);
					assert this.hasNode(u);
					this.removeNode(u);
				}
				
				// remove the self loop on the first node to which the component is collapsed now
				Unit u = scc.get(0);
				for (Unit su : suc2add) {
					if (u==su) continue;
					//assert cfg.getSuccsOf(u).contains(su);
					if (cfg.hasNode(su)) cfg.addToSuccs(u, su);
					//assert this.getSuccsOf(u).contains(su);
					this.getSuccsOf(u).add(su);
				}
				for (Unit pu : pred2add) {
					if (u==pu) continue;
					//assert cfg.getPredsOf(u).contains(pu);
					if (cfg.hasNode(pu)) cfg.addToPreds(u, pu);
					//assert this.getPredsOf(u).contains(pu);
					this.getPredsOf(u).add(pu);
				}
				
				if (cfg.getSuccsOf(u).contains(u)) {
					cfg.removeFromSuccs(u, u);
					this.getSuccsOf(u).remove(u);
				}
				if (cfg.getPredsOf(u).contains(u)) {
					cfg.removeFromPreds(u, u);
					this.getPredsOf(u).remove(u);
				}
			}
		}
	} // - createCCFGs
	
	/** put all together: build the ICCFG from the scratch */
	public void buildGraph() {
		/*
		PointsToAnalysis pa = Scene.v().getPointsToAnalysis(); //new PAG(new SparkOptions(new LinkedHashMap()));
		CallGraphBuilder cgBuilder = new CallGraphBuilder(pa);
		cgBuilder.build();
		cg = cgBuilder.getCallGraph();
		*/
		
		collectCFGs();
		copyCFGs();
		addCallEdges();
		createICCFG();
		
		//connectSetterToGetters();

		buildHeadsAndTails();
		
		// clear accessory nodes (virtual ENTRY and EXIT nodes)
		/*
		for (SootMethod m : mToCfg.keySet()) {
			mToCfg.get(m).removeVirtualNodes();
		}*/
	} // buildGraph
	
	/** dump the graph structure of the Inter-procedural CFG */
	public void dumpGraph() {
		super.dumpGraph("=== Interprocedural Component CFG ===");
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///                                           VISUALIZATION OF ICFG (w.r.t DotGraph@Graphviz)
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/** Serialize the ICFG into DotGraph format, which can be rendered by Graphviz Dot offline */
	public int visualizeGraph(String filename) {
		return super.visualizeGraph(filename, "Interprocedural Component CFG");
	}
}

/* vim :set ts=4 tw=4 tws=4 */
