package disttaint;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import profile.BranchInstrumenter;
import soot.jimple.Stmt;
import dua.global.ProgramFlowGraph;
import dua.method.CFGDefUses.Branch;
import dua.util.Util;
import fault.StmtMapper;
import MciaUtil.RDFCDBranchEx;
import profile.BranchReporter;

public class dt2BrMonitor {
	/* debug flag: e.g. for dumping event sequence to human-readable format for debugging purposes, etc. */
	protected static boolean debugOut = false;
	public static void turnDebugOut(boolean b) { debugOut = b; }
	
	/* a flag ensuring the initialization and termination are both executed exactly once and they are paired*/
	protected static boolean bInitialized = false;
	
	private final static RDFCDBranchEx rdfCDBranchAnalyzer = RDFCDBranchEx.inst();

	/** if remove repeated branches in terms of same targets */
	public static boolean removeRepeatedBrs = true;
	
	protected static int[] paramArrayOfString=new int[Integer.MAX_VALUE];
	
	/* for DUAF/Soot to access this class */
	public static void __link() {    }
	/* initialize the two maps and the global counter upon the program start event */		
	public synchronized static void initialize() throws Exception{
		bInitialized = true;
		System.out.println("**************dt2BrMonitor::initialize() 0th");
//		dumpBranchCDStmts();
		instrumentAllBranches();
		
		for (int i=0; i<Integer.MAX_VALUE; i++)
			paramArrayOfString[i]=0;
		/** add hook to catch SIGKILL/SIGTERM */
		Runtime.getRuntime().addShutdownHook( new Thread()
        {
          public void run()
          {
            System.out.println( "Shutdown signal caught!" ) ;
        	/** guarantee that the trace, if any collected, gets dumped */
        	if (debugOut) {
        		System.out.println("\nDumping statement trace of current process execution ...");
        	}
    		try {
				dt2BrMonitor.terminate("Forced upon external termination");
			} catch (Exception e) {
				e.printStackTrace();
			}
          }
        } ) ;
		
		String debugFlag = System.getProperty("ltsDebug");
		System.out.println("debugFlag="+debugFlag);
		if (null != debugFlag) {
			debugOut = debugFlag.equalsIgnoreCase("true");
		}
		
		System.out.println("debugOut="+debugOut);
		System.out.println("dt2BrMonitor starts working ......");
		
	}
	
	public synchronized static void terminate(String where) throws Exception {
		/** NOTE: we cannot call simply forward this call to super class even though we do the same thing as the parent, because
		 * we need take effect the overloaded SerializeEvents() here below 
		 */
		//Monitor.terminate(where);
		if (bInitialized) {
			bInitialized = false;
		}
		else {
			return;
		}		
		System.out.println("dt2BrMonitor termonitors");
//		int branchLineNum=dtUtil.getLineNum("entitystmt.out.branch");
//		int[] paramArrayOfString=new int[branchLineNum];
	    new BranchReporter(true).report(paramArrayOfString);
	}
	
	public synchronized static void enter(String methodname){
		System.out.println("\n enter(String methodname)="+methodname);
	}

	public synchronized static void returnInto(String methodname, String calleeName){

	}
	
	/** instrument branch coverage monitors for all branches */
	protected static int instrumentAllBranches() {
		//System.out.println("instrumentAllBranches 0th");
		List<Branch> allBranches = rdfCDBranchAnalyzer.getAllBranches();
		//System.out.println("instrumentAllBranches 1th");
		if (removeRepeatedBrs) {
			allBranches = rdfCDBranchAnalyzer.getAllUniqueBranches();
		}
		//System.out.println("instrumentAllBranches 2th");
		
		// instrument using DuaF facilities
		BranchInstrumenter brInstrumenter = new BranchInstrumenter(true);
		//System.out.println("instrumentAllBranches 3th");
		brInstrumenter.instrumentDirect(allBranches, ProgramFlowGraph.inst().getEntryMethods());
		//System.out.println("instrumentAllBranches 4th");
		
		return 0;
	} // instrumentAllBranches
}
