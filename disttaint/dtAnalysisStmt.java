package disttaint;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import soot.jimple.Stmt;

public class dtAnalysisStmt {
	static Set<String> changeSet = new LinkedHashSet<String>();
	static HashMap<String, Integer> impactSet_dist = new HashMap<String, Integer> ( );
	
	static boolean debugOut = true;
	static String method1 = "";
	static String stmt1 = "";
	static String method2 = "";
	static String stmt2 = "";
	static DVTNode startNode=null;
	static DVTNode endNode=null;
	static String sourceMessage="";
	static String sinkMessage="";
	static ArrayList stmtStrs=new ArrayList();
//	// or compute impact sets for multiple queries at the same time when traversing the execution trace for only once
//	static boolean matchingDynVTGForAllQueries = false;
//	// include the pruning approach just for a comparison
//	static boolean pruningDynVTGForAllQueries = false;
//	
//	/** if applying runtime statement coverage information to prune statements not executed, examined per test case */
//	public static boolean applyStatementCoverage = false;
//	/** prune non-covered/non-aliased nodes and edges prior to or after basic querying process: 
//	 * both are equivalent in terms of eventual impact set but can be disparate in performance
//	 */
//	public static boolean postPrune = true;
//	
//	/** if applying runtime object alias checking to prune heap value edges on which the source 
//	 * and target nodes are not dynamically aliased 
//	 */
//	public static boolean applyDynAliasChecking = false;
//	/** if pruning based on the dynamic alias information at the method instance level, or just the method level */
//	public static boolean instancePrune = true; 
//	static Map<String, Set<String>> localImpactSets = new LinkedHashMap<String, Set<String>>();
//	// distEA variables
//	static boolean separateReport = true;
	static boolean reportCommon = false;
	static boolean strictComponent = false; // strictly two different components won't have common traces --- they have to communicate by message passing
	static boolean improvedPrec = true; // choose between the purely EA-based and precision-improved versions
//	static boolean runDiver = false;
	static Set<String> impactSet = new LinkedHashSet<String>();
	
	public static void main(String args[]){
	
		if (args.length < 3) {
			System.err.println("Too few arguments: \n\t " +
					"DiverAnalysis changedMethods traceDir binDir [numberTraces] [debugFlag]\n\n");
			return;
		}
		//System.out.println("0th\n");
		String query = args[0]; // tell the changed methods, separated by comma if there are more than one
		String traceDir = args[1]; // tell the directory where execution traces can be accessed
		String binDir = args[2]; // tell the directory where the static value transfer graph binary can be found

//		String query="<C: void main(java.lang.String[])> - virtualinvoke r2.<B: void printString(int,java.lang.String,java.lang.String)>(-1, \"positive\", \"negative\");<B: void printString(int,java.lang.String,java.lang.String)> - r1 := @parameter1: java.lang.String";
//		String binDir="C:\\TEST\\DTInstrumented";
		if (debugOut)
		{
			System.out.println(" query="+query+" binDir="+binDir);
		}			
		method1=dtStmtUtil.getFirstMethod(query);
		stmt1=dtStmtUtil.getFirstStmt(query);
		method2=dtStmtUtil.getSecondMethod(query);
		stmt2=dtStmtUtil.getSecondStmt(query);
		if (args.length > 3) {
			debugOut = args[3].equalsIgnoreCase("-debug");
		}
		if (args.length > 4) {
			improvedPrec  = args[4].startsWith("-prec");
		}
		if (debugOut)
		{
			System.out.println("Source: "+method1+" - "+stmt1);
			System.out.println("Sink: "+method2+" - "+stmt2);
		}
		if (dtStmtUtil.initVTG(binDir) != 0) {
			// something wrong during the initialization of the dynamic graph
			System.out.println("dtStmtUtil.initVTG(binDir) Failed!");
			return;
		}
		stmtStrs=dtStmtUtil.getStmtStrsFromFile(binDir+File.separator+"stmtids.out");
		System.out.println("stmtStrs="+stmtStrs+" dvtg.nodeSet.size()="+dtStmtUtil.dvtg.nodeSet().size());
		if (findStartEndNodes()!=0)  {
			// No source or sink
			return;
		}
		sourceMessage=dtStmtUtil.getSuccessorMessageToWrite(startNode,stmtStrs,true);
		sinkMessage=(dtStmtUtil.getPredecessorMessageToRead(endNode,stmtStrs,true));  //.replace(" <-- "," --> "));
		String changedMethods=method1;
		String sink=method2;
		if (debugOut)
			System.out.println("changedMethods="+changedMethods+" sink="+sink);
		//impactSet.addAll(dtStmtUtil.sourceMethods);
		//impactSet.addAll(dtStmtUtil.sinkMethods);
		//dist works
				try {
					// process the given query directly
					File funclist = new File(changedMethods);
					//if (!funclist.isFile()) 
					{						
						if (debugOut)						
							System.out.println("separateReport");
						
						separateParseTraces(changedMethods, traceDir,sink);
						
						
						//return;
					}			

				} catch (Exception e) {
					e.printStackTrace();
				}		
	}
//	public static int initVTG(String binDir) {		
//		dvtg.setSVTG(binDir+File.separator+"staticVtg.dat");
//		if (0 != dvtg.initializeGraph(debugOut)) {
//			System.out.println("Unable to load the static value transfer graph, aborted now.");
//			return -1;
//		}
//		DynTransferGraph.reachingImpactPropagation = false;
//		return 0;
//	}
	public static int findStartEndNodes()
	{
		String methodStr="";
		String stmtStr="";
		try {
			for (DVTNode dn : dtStmtUtil.dvtg.nodeSet()) {				
				methodStr=dtStmtUtil.dvtg.idx2method.get(dn.getMethod());
				stmtStr=(String) stmtStrs.get(dn.getStmt());
//				if (debugOut)
//				{
//					System.out.println(" methodStr - stmtStr ="+methodStr+" - "+stmtStr);
//				}	
				if (methodStr.equals(method1)  &&  stmtStr.equals(stmt1))
				{
					startNode=new DVTNode(dn.getVar(),dn.getMethod(),dn.getStmt());
				}
				if (methodStr.equals(method2)  &&  stmtStr.equals(stmt2))
				{
					endNode=new DVTNode(dn.getVar(),dn.getMethod(),dn.getStmt());
				}
				if (startNode != null  && endNode != null)
					break;
			}
		}
		catch (Exception e) { 
			System.err.println("Exception e="+e);
			return -1;
		}	
		if (startNode == null)  {
			System.out.println("No Source Node: "+method1+" - "+stmt1);
			return -2;
		}	
		if (endNode == null)  {
			System.out.println("No Sink Node: "+method2+" - "+stmt2);
			return -3;
		}	
		return 0;
	}
	
	public static void separateParseTraces(String changedMethods, String traceDir,String sinks) {
		int tId;
		List<String> Chglist = dua.util.Util.parseStringList(changedMethods, ';');
		if (Chglist.size() < 1) {
			// nothing to do
			return;
		}
		//for (tId = 1; tId <= nExecutions; ++tId)
		for (tId = 1; tId <= 1; ++tId) {
			//String CLT = "";
			Integer tsCLT = Integer.MAX_VALUE;
			try 
			{
				String dnSource = traceDir  + File.separator + "test" + tId + File.separator;
				HashMap<String, Integer> F = new HashMap<String, Integer> ( );
				HashMap<String, Integer> L = new HashMap<String, Integer> ( );
				
				Map<String, HashMap<String, Integer>> f2L = new HashMap<String, HashMap<String, Integer>> ( );
				Map<String, HashMap<String, Integer>> f2F = new HashMap<String, HashMap<String, Integer>> ( );
				Map<String, HashMap<String, Integer>> f2S = new HashMap<String, HashMap<String, Integer>>();
				
				//Set<String> covered = new HashSet<String> ( );
								
				if (debugOut) {
					System.out.println("\nDeserializing event maps from " + dnSource);
				}
				// 1. distEA reconstructs the two event maps from the serialized execution trace associated with this test
				mergeTraces(new File(dnSource), F, L);				
				// -- DEBUG
				if (debugOut) {
					System.out.println("\n[ First events ]\n" + F );
					System.out.println("\n[ Last events ]\n" + L );
				}				
				// dist
				File dn = new File(dnSource);
				// load trace of all processes into memory
				long fixtime = System.currentTimeMillis();
				for (String fname : dn.list()) {
					File loc = new File(dn, fname);
					if (loc.isFile() && loc.getName().endsWith(".em")) {
						FileInputStream fis = new FileInputStream(loc);
						ObjectInputStream ois = new ObjectInputStream(fis);
						HashMap<String, Integer> curF =  (HashMap<String, Integer>) ois.readObject();
						HashMap<String, Integer> curL =  (HashMap<String, Integer>) ois.readObject();
						
						if (improvedPrec) {
							HashMap<String, Integer> _curS =  (HashMap<String, Integer>) ois.readObject();
							HashMap<String, Integer> curS = new HashMap<String, Integer>();
							for (Map.Entry<String, Integer> entry : _curS.entrySet()) {
								String k = entry.getKey().trim().replace("\0", "");
								Integer v = entry.getValue();
								curS.put(k, v);
							}
							f2S.put(loc.getAbsolutePath(), curS);
						}
						
						f2L.put(loc.getAbsolutePath(), curL);
						f2F.put(loc.getAbsolutePath(), curF);
						fis.close();
						ois.close();
					}
				}
				if (improvedPrec && debugOut) {
					System.out.println("Content of Sender map:");
					for (String tr : f2S.keySet()) {
						System.out.println("in trace of " + tr);
						for (String sender : f2S.get(tr).keySet()) {
							System.out.println(sender + "\t" + f2S.get(tr).get(sender));
						}
					}
				}
				fixtime = System.currentTimeMillis() - fixtime;
				
				// compute impacts in local and external processes
				for (String tf : f2F.keySet()) {
					HashMap<String, Integer> curF =  f2F.get(tf);
					HashMap<String, Integer> curL = f2L.get(tf);
					
					String ProcessI = null;
					if (improvedPrec) {
						HashMap<String, Integer> curS = f2S.get(tf);
						assert curS.containsValue(Integer.MAX_VALUE);
						for (String pid : curS.keySet()) {
							if (curS.get(pid)==Integer.MAX_VALUE) {
								ProcessI = pid;
								break;
							}
						}
						assert ProcessI != null;
					}					
					List<String> localChgSet = new ArrayList<String>();
					Set<String> localImpactSet = new LinkedHashSet<String>();					
					tsCLT = computeProcesTrace(Chglist, curF, curL, localChgSet, localImpactSet);		
					// impacted methods in other processes
					Set<String> exImpactSet = new LinkedHashSet<String>();
					int excludedN = 0;
					for (String tl : f2L.keySet()) {
						if (tl.equalsIgnoreCase(tf)) { continue; }
						if (strictComponent) {
							Set<String> lptrace = new LinkedHashSet<String>(f2F.get(tf).keySet());
							lptrace.retainAll(f2L.get(tl).keySet());
							if (!lptrace.isEmpty()) continue;
						}
						for (String m : f2L.get(tl).keySet()) {
							if (f2L.get(tl).get(m) >= tsCLT) {
								if (improvedPrec) {
									HashMap<String, Integer> curS = f2S.get(tl);
									if (debugOut) {
										if (!curS.containsKey(ProcessI)) {
											System.out.println(ProcessI + " is NOT found in the sender map..., is that correct?");
											System.out.println(ProcessI + " is NOT in the set of " + curS.keySet());
										}
										else {
											System.out.println(ProcessI + " is INDEED found in the sender map, is that correct?");
										}
									}
									if (curS.containsKey(ProcessI) && f2L.get(tl).get(m)>=curS.get(ProcessI)) {
										exImpactSet.add(m);
									}
									else {
										excludedN ++;	
									}
								}
								else {
									exImpactSet.add(m);
								}
							}
						}
						// replace distEA local sets with method1 sets
						
						localChgSet.clear();
						localImpactSet.clear();
						localChgSet.addAll(dtStmtUtil.sourceMethods);
						localImpactSet.addAll(dtStmtUtil.sourceMethods);
		

						changeSet.addAll(localChgSet);
						impactSet.addAll(localImpactSet);
					}
					

					
					if (debugOut)
					{
						System.out.println("[local impact Set ] size = " + localImpactSet.size());
						System.out.println("\n[localImpactSet]\n" + localImpactSet );						
						System.out.println(" tf= " + tf);
						System.out.println("[external impact set ] size = " + exImpactSet.size());
						System.out.println("\n[exImpactSet]\n" + exImpactSet );
						System.out.println(excludedN + " methods were excluded from external impact set due to the precision improvement.");
					}
					if (exImpactSet!=null && !exImpactSet.isEmpty())
						impactSet.addAll(exImpactSet);
				}

				if (debugOut)  {
					System.out.println("[impact set ] size = " + impactSet.size());
					System.out.println("\n[impact set ]\n" + impactSet);
				}	
				Iterator iter = L.entrySet().iterator();
				while (iter.hasNext()) {
					Map.Entry entry = (Map.Entry) iter.next();
//					if (debugOut) 
//						System.out.println("L Ket ="+ entry.getKey()+" Value ="+ entry.getValue());
					String methodName=entry.getKey().toString();
					if (impactSet.contains(methodName) || impactSet.contains(methodName.toLowerCase()) || impactSet.contains(methodName.toUpperCase()))
					{
						impactSet_dist.put(methodName, Integer.parseInt(entry.getValue().toString()));
					}
				}
				if (debugOut)
				{	
					System.out.println("[impactSet_dist ] size = " + impactSet_dist.size());					
					System.out.println("\n[impactSet_dist]\n" + impactSet_dist);
				}
				// transfer impactSet to resultList to sort
				List<Map.Entry<String, Integer>> resultList = new ArrayList<>();
		        for(Map.Entry<String, Integer> entry : impactSet_dist.entrySet()){
		             resultList.add(entry); 
		        }
		        //System.out.println("[List ] size = " + resultList.size());
		        // sort the resultList
		        resultList.sort(new Comparator<Map.Entry<String, Integer>>(){
		              @Override
		               public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
		                    return o1.getValue()-o2.getValue();} 
		        }); 
				
				List<String> sinklist = dua.util.Util.parseStringList(sinks, ';');
				if (sinklist.size() < 1) {
					System.out.println("\n [All results from source "+ changedMethods+" ]:" + resultList.size());
					for(Map.Entry<String, Integer> entry: resultList){
			              //System.out.println(entry);
			        	System.out.print(entry.getKey()+ " ->  ") ;
			        }
			        System.out.println(" ");
				}
				else
				{
					int pathSize=0;
					String midMethod="";
					String midStr="";
					boolean hadPrint=false;  //did print?
					for(int i = 0 ; i < sinklist.size() ; i++) {
						System.out.println("\n [The result from source "+ changedMethods+ " to the sink "+ sinklist.get(i)+ "]:");
						
						System.out.println(sourceMessage);
						if (!impactSet.contains(sinklist.get(i)))
						{
							System.out.println(" There is no remote flow path!\n");
							System.out.println(sinkMessage);
							continue;
						}
						System.out.print("--> \n");
						for(Map.Entry<String, Integer> entry: resultList){
							//System.out.println(" entry.getKey()= "+ entry.getKey()+ " sinklist.get(i))= "+ sinklist.get(i));
							hadPrint=false;
							midMethod=entry.getKey().toString();
							//System.out.println("midMethod="+midMethod);
							if (midMethod.length()<1)
								continue;
							if (!midMethod.equals(method1) && !midMethod.equals(method2) && !midMethod.equals(sinklist.get(i))) 
							{
								midStr=dtStmtUtil.getMethodStmtStr(midMethod, stmtStrs);
								if (midStr.length()>1)
								{	
									pathSize++;
									hadPrint=true;
									System.out.print(midStr);
								}
							}
							if (hadPrint)
								System.out.print("--> \n");
				        }
						
						System.out.println(sinkMessage);
				        //System.out.println("\n [The path length]: "+pathSize+"\n");
					}
				}
				
			}
//			catch (FileNotFoundException e) {
//				break;
//			}
//			catch (ClassCastException e) {
//				System.err.println("Failed to cast the object deserialized to HashMap<String, Integer>!");
//				return;
//			}
//			catch (IOException e) {
//				throw new RuntimeException(e); 
//			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (debugOut) {
			System.out.println(--tId + " execution traces have been processed.");
		}

	}
	
	@SuppressWarnings("unchecked")
	public static void mergeTraces(File loc, HashMap<String, Integer> F, HashMap<String, Integer> L)  throws IOException, ClassNotFoundException {
		if (loc.isFile() && loc.getName().endsWith(".em")) {
			FileInputStream fis = new FileInputStream(loc);
			ObjectInputStream ois = new ObjectInputStream(fis);
			HashMap<String, Integer> curF =  (HashMap<String, Integer>) ois.readObject();
			HashMap<String, Integer> curL =  (HashMap<String, Integer>) ois.readObject();
			
			F.putAll(curF);
			L.putAll(curL);
			
			fis.close();
			ois.close();
			
			return;
		}
		
		if (!loc.isDirectory()) { return; }
		
		for (String fname : loc.list()) {
			mergeTraces (new File(loc, fname), F, L);
		}
	}
	
	/** compute impacts from the trace dumped by one OS process with respect to a test input */ 
	private static int computeProcesTrace(List<String> Chglist, HashMap<String, Integer> F, 
			HashMap<String, Integer> L, List<String> localChgSet, Set<String> localImpactSet) {
		// determine the CLT (Change with Least Time-stamp in F)
		String CLT = "";
		Integer tsCLT = Integer.MAX_VALUE;
		for (String chg : Chglist) {
			for (String m : F.keySet()) {
				if ( !m.toLowerCase().contains(chg.toLowerCase()) && 
						!chg.toLowerCase().contains(m.toLowerCase()) ) {
					// unmatched change specified even with a very loose matching
					continue;
				}
				localChgSet.add(m);
				if (F.get(m) <= tsCLT) {
					tsCLT = F.get(m);
					CLT = m;
				}
			}
		}
		// compute the impact set with respect to this execution trace
		for (String m : L.keySet()) {
			if (L.get(m) >= tsCLT) {
				localImpactSet.add(m);
			}
		}
		return tsCLT;
	}
}